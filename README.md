# caspo



## Installation

```
docker pull registry.gitlab.inria.fr/dyliss/caspo/caspo:latest
```


## Code

[https://github.com/bioasp/caspo](https://github.com/bioasp/caspo)


## Documentation

[https://caspo.readthedocs.io/en/latest](https://caspo.readthedocs.io/en/latest)


## License

GPL-3.0
